import pytest

from takehome.agent import Agent


@pytest.fixture
def agent():
    # this one if free: we use it for the tests
    return Agent(model="mixtral-8x7b-32768", platform="groq")


def test_agent(agent):
    dummy_query = "please say the word 'yes' in French."
    dummy_response = agent.chat(dummy_query)
    assert "oui" in dummy_response.lower()
