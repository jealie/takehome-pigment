import pytest

from takehome.pdf import read_doc


def test_read_doc():
    txt = read_doc("data/test.pdf", 0, 1, 60)
    assert txt == "Dummy PDF page made for testing the takehome."
