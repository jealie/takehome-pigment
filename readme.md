# 🌈 Pigment's Takehome by Jean 👨🏻‍💻

The repo contains everything to reproduce the takehome work, including the data used and my OpenAI and Groq personal keys (please do not use them too much!).

Code-wise, it provides three different Python documents:

- the documented code as a tested package (`takehome` & `tests` directories)

- the exploratory notebooks (`notebooks` directory)

- a Streamlit interface (`experimental_chat.pdf`)

The [summary report](https://gitlab.com/jealie/takehome-pigment/-/raw/master/report.pdf) is also included here.

## Repo organization


```
├── data                                  # => The data used (pdf, excel, ...)
├── noteboooks                            # => The notebooks
│   ├── exploratory_analysis.ipynb
│   ├── exploratory_forecast.ipynb
│   ├── exploratory_pdf_conversion.ipynb
│   └── exploratory_rag.ipynb
├── takehome                              # => Routines of takehome Python package
│   ├── agent.py
│   └── pdf.py
├── tests                                 # => Tests of these routines
│   ├── agent.py
│   └── pdf.py
├── experimental_chat.py                  # => Streamlit Chat Interface
├── Dockerfile/Makefile                   # => Dockerfile & Makefile (see below)
└── report.pfd                            # => The summary report
└── readme.md                             # => This file
```


## Managing the docker with the Makefile

Type `make` to display the list of possible options:

```
  help                           Display this help
  build                          Build the docker
  run-jupyterlab                 Run a jupyterlab session with the docker
  run-streamlit                  Run a streamlit with the docker
  test                           Run the test suite
```

### `make build`

This builds a docker containing all Python requirements, as well as Jupyterlab and Streamlit.

The docker also provides the code as a package, meaning that one can load its routines with `from takehome.agent import ForecastingAgent` (for example).

### `make run-jupyterlab`

Run a jupyterlab session on port 8500, which you can access from the host at address https://localhost:8500/

In this jupyterlab session, all the notebooks can be run and their results can be reproduced.

### `make run-streamlit`

Launch a streamlit instance on port 8501, also accessible from the host at address: http://localhost:8501/

### `make test`

Demonstrate tests with `pytest` of some routines of `takehome/pdf.py` and `takehome/agent.py`
