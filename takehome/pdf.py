import os
import pypdfium2 as pdfium

from tqdm.auto import tqdm


def read_doc(path: str | os.PathLike, start: int = 0, end: int = None, margin=0):

    pdf = pdfium.PdfDocument(path)

    if end is None:
        end = len(pdf)

    txt = []
    for i in tqdm(range(start, end)):
        txt.append(
            pdf[i]
            .get_textpage()
            .get_text_bounded(
                left=margin,
                bottom=pdf[i].get_height() - margin,
                right=pdf[i].get_width() - margin,
                top=margin,
            )
        )

    raw_text = (
        " ".join(txt).replace("\r\n", "\n").replace("❦\n", "").replace("\x02", "")
    )

    return raw_text
