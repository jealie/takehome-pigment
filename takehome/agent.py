import pandas as pd
import json
import copy
import plotly.express as px


class Agent:

    def __init__(self, model: str, platform: str, prompt_history: bool = True):
        self.platform = platform
        if platform == "openai":
            from openai import OpenAI

            self.client = OpenAI(
                api_key="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"
            )
        elif platform == "groq":
            from groq import Groq

            self.client = Groq(
                api_key="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"
            )
        self.messages = []
        self.model = model
        self.prompt_history = prompt_history

    def set_system(self, prompt: str):
        """
        Sets the system prompt

        Parameters
        ----------
        prompt : str
            The system prompt for the LLM

        """
        self.messages.append({"role": "system", "content": prompt})

    def chat(self, prompt: str, return_as_json: bool = False):
        """
        One line in a dialog with a LLM.

        Parameters
        ----------
        prompt : str
            The prompt for the LLM
        return_as_json : book
            whether of not to return as a json-parsable object (works only with OpenAI, not Groq)

        Returns
        -------
        str
            The LLM answer

        """
        if return_as_json:
            if self.platform == "groq":
                raise RuntimeError("Groq does not handle the `json_object` option")
            chat_completion = self.client.chat.completions.create(
                messages=self.messages + [{"role": "user", "content": prompt}],
                model=self.model,
                response_format={"type": "json_object"},
            )
        else:
            chat_completion = self.client.chat.completions.create(
                messages=self.messages + [{"role": "user", "content": prompt}],
                model=self.model,
            )
        response = chat_completion.choices[0].message.content
        if self.prompt_history:
            self.messages.append({"role": "system", "content": response})
        return response


class ForecastAgent(Agent):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def load_pnl_data(self):
        """
        Loads the pre-processed PnL data for future use

        """
        self.pnl = pd.read_csv("/workspace/data/pnl.csv")
        self.pnl["Month"] = [str(m)[:7] for m in self.pnl["Month"]]
        self.pnl.set_index("Month", inplace=True)
        self.categs = [
            "Transactions",
            "Revenue",
            "Cost of Services",
            "Credit Card Fees",
            "Gross Profit",
            "OPEX",
        ]

        system_prompt = """You are an expert accountant and you are given two years of historical PnL data for a company. Your task is to forecast the PnL one year in the future.
        
        """
        system_prompt += f"Below is the historical data as a json object:\n"
        system_prompt += str(self.pnl[self.categs].to_dict())
        self.set_system(system_prompt)

    def forecast_pnl(self, ret, variable_scenario):
        historical = copy.deepcopy(self.pnl[self.categs])
        historical["what"] = "historical"
        forecast = pd.DataFrame(json.loads(ret))
        forecast["what"] = "LLM forecast"
        df = pd.concat([historical, forecast])
        fig = px.line(
            df[variable_scenario["variable"]],
            title=variable_scenario.get("scenario", ""),
        )
        return fig
