import streamlit as st
import plotly.graph_objs as go
from takehome.agent import Agent, ForecastAgent

import json

from langchain_chroma import Chroma
from langchain_community.embeddings.sentence_transformer import (
    SentenceTransformerEmbeddings,
)


"""
# 🌈 Pigment's Takehome by Jean 👨🏻‍💻

Type in the box below to chat with the bot:
"""

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

# Initialize agents
if "branch_agent" not in st.session_state:
    # This agent decides if it is a RAG task or a forecast task
    st.session_state.branch_agent = Agent(
        model="gpt-3.5-turbo", platform="openai", prompt_history=False
    )
    st.session_state.branch_agent.set_system(
        """You are a very simple agent with access to excerpts of the book FINANCIAL PLANNING & ANALYSIS AND PERFORMANCE MANAGEMENT, as well as with the PnL of the company Happy Trotters. You indicates as JSON if the user instructed you to forecast the PnL or any quantitative data about the company Happy Trotter (in which case you return {"output": "forecast"}), or if the user wants the kind of generalist information you could find in the book that was mentionned in the previous sentence ({"output": "RAG"}). If you have no idea, then defaults to {"output": "RAG"}. For example, if the user asks you to "forecast the EBITDA of Happy Trotters" or "study the scenario where there is a market crash for Happy Trotters" or "draw a graphical representation with x as time and y as dollar of credit cards fees for Happy Trotters", then your answer would be {"output": "forecast"}. Other examples could be if the user asks you to "provide a definition for the EBITDA" or "tell them what are the human biases when forecasting" or "What is a PnL", then you output {"output": "RAG"}."""
    )
if "scenario_variables_selection" not in st.session_state:
    # This agent decides what is the variable and the scenario (if it is a forecast task)
    st.session_state.scenario_variables_selection = Agent(
        model="gpt-3.5-turbo", platform="openai", prompt_history=False
    )
    st.session_state.scenario_variables_selection.set_system(
        """You are an expert company strategist and you know how to forecast PnL of the company Happy Trotters. You indicate as a JSON which variable the user wants to forecast among the set of following variables: ['Transactions', 'Revenue', 'Cost of Services', 'Credit Card Fees', 'Gross Profit', 'OPEX', 'EBITDA']. The optional key "scenario" may specify a desired scenario with a str truncated to its first 500 characters. Note that there can be only one forecasted variable: if the user specify two or more variables, return {"error": "too many variables asked"}.

For example, if the user asks you to "forecast the EBITDA of Happy Trotters", then your answer should be {"variable": "EBITDA"}. If the user asks you to "forecast the EBITDA under the scenario of a worldwide recession", then your answer should be {"variable": "EBITDA", "scenario": "worldwide recession"}. If the user asks you to "simulate the future of the Credit Card Fees for Happy Trotters", then your answer should be {"variable": "Credit Card Fees"}. If the user asks you to "simulate the future of the Revenue for Happy Trotters assuming very favorable market conditions" then your answer should be {"variable": "Revenue", "scenario": "very favorable market conditions"}. But if the user asks about variables that is not mentionned in the list, such as "forecast the number of employees", then you return a helpful error message, such as {"error": "I do not know the number of employees"}. If the user asks you for two or more variables, then you should return something like {"error": "too many variables asked"}."""
    )
if "forecast_agent" not in st.session_state:
    # This agent performs the LLM forecast
    st.session_state.forecast_agent = ForecastAgent(
        model="gpt-3.5-turbo", platform="openai", prompt_history=False
    )
    st.session_state.forecast_agent.load_pnl_data()
if "RAG_agent" not in st.session_state:
    # This agent performs the RAG over the textbook
    # This is the only agent with access to its prompt history
    st.session_state.RAG_agent = Agent(model="gpt-3.5-turbo", platform="openai")
    st.session_state.RAG_agent.set_system(
        """You are a smart assistant, very knowledgeable in financial planning and analysis. You love to quote the book FINANCIAL PLANNING & ANALYSIS AND PERFORMANCE MANAGEMENT."""
    )

# Initialize DB
if "db" not in st.session_state:
    # Loads the chroma db embeddings
    embedding_function = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")
    st.session_state.db = Chroma(
        persist_directory="data/chromadb", embedding_function=embedding_function
    )


def add_retrieved_data(query, db):
    queried_docs = db.similarity_search(query, k=5)
    return "\n===\n".join([q.page_content for q in queried_docs]) + "\n==="


# Display chat messages and graphs from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        if isinstance(message["content"], str):
            st.markdown(message["content"])
        else:
            st.plotly_chart(message["content"])

# React to user input
if prompt := st.chat_input("Try me... but not too hard! I am very experimental."):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

# Display assistant response in chat message container
if prompt is not None:
    print(f"{prompt=}")
    with st.chat_message("assistant"):
        is_graph = st.session_state.branch_agent.chat(prompt, return_as_json=True)
        print(f"{is_graph=}")
        try:
            graph_mode = "forecast" in json.loads(is_graph).get("output", "").lower()
        except:
            graph_mode = False
        print(f"{graph_mode=}")
        if graph_mode:
            graph_variables = st.session_state.scenario_variables_selection.chat(
                prompt, return_as_json=True
            )
            print(f"{graph_variables=}")
            graph_variables = json.loads(graph_variables)
            graph_prompt = "Forecast this PnL one year in the future. Be as sophisticated as you can based on the data at hand. Write the data using the same format as given in input, i.e. as a json where the keys are the different categories of the PnL, and each key contains entries formatted as {'YYYY-MM': 'FORECAST'}."
            if "scenario" in graph_variables.keys():
                graph_prompt += f"You simulate specifically the following scenario: '{graph_variables}'. Stick to it as closely as possible."
            graph_return = st.session_state.forecast_agent.chat(
                graph_prompt, return_as_json=True
            )
            print(f"{graph_return=}")
            fig = st.session_state.forecast_agent.forecast_pnl(
                graph_return, variable_scenario=graph_variables
            )
            st.plotly_chart(fig)
            st.session_state.messages.append({"role": "assistant", "content": fig})
        else:
            enriched_prompt = (
                f"\nYou recall interesting excerpts from your favorite book FINANCIAL PLANNING & ANALYSIS AND PERFORMANCE MANAGEMENT:\n{add_retrieved_data(prompt, st.session_state.db)}.\n\nNow, answer the question:\n\n"
                + prompt
            )
            response = st.session_state.RAG_agent.chat(
                enriched_prompt, return_as_json=False
            )
            print(f"{response=}")
            st.markdown(response)
            st.session_state.messages.append({"role": "assistant", "content": response})
